/* global L */

// seznam z markerji na mapi
var markerji = [];
var a = [];

var minimum=-1,mx,my;
var znacilnosti;

var mapa;

const UKC_LAT = 46.054306;
const UKC_LON = 14.521081;


/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [UKC_LAT, UKC_LON],
    zoom: 12
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Ročno dodamo fakulteto za računalništvo in informatiko na mapo
  //dodajMarker(UKC_LAT, UKC_LON, "Univerzitetni klinični center Ljubljana", "Zaloška cesta 7, Ljubljana");

  // Objekt oblačka markerja
  var popup = L.popup();

//obarvaj najblizje poligone
var latlng=0;

  function obKlikuNaMapo(e) {
    latlng = e.latlng;
    minimum=-1;
    var polygon;

    for (var i = 0; i < znacilnosti.length; i++) {


           if( znacilnosti[i].geometry.coordinates[0].length >= 1)
                var jeObmocje =znacilnosti[i].geometry.coordinates[0][0].length==2;
            else
                jeObmocje=0;
           
              // pridobimo koordinate
          
          
          var lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] : 
              0;
          var lat= jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] : 
              0;
           
           if(minimum<"0"){
             mx=lng;
             my=lat;
             minimum=latlng.distanceTo(L.latLng(my, mx));
           }
           else{
             if(minimum>latlng.distanceTo(L.latLng(lat, lng))){
               mx=lng;
               my=lat;
               minimum=latlng.distanceTo(L.latLng(lat, lng));
             }
           }

           }
        
        for (var i = 0; i < znacilnosti.length; i++) {

          jeObmocje = 
             typeof(znacilnosti[i].geometry.coordinates[0]) == "object";
              // pridobimo koordinate
              

          lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] : 
              znacilnosti[i].geometry.coordinates[0];
          lat= jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] : 
              znacilnosti[i].geometry.coordinates[1];
              
      
          for(var j=0;j<znacilnosti[i].geometry.coordinates.length;j++){

            for(var k=0;k<znacilnosti[i].geometry.coordinates[j].length;k++){
              if(!a[k])
                a[k]=[];
              a[k][0] =znacilnosti[i].geometry.coordinates[j][k][1];
              a[k][1] =znacilnosti[i].geometry.coordinates[j][k][0];
            }

            if(a.length>0){

              if(1110000>=(L.latLng(my,mx)).distanceTo(L.latLng(lat, lng))){
            
                  polygon = L.polygon(a, {color: 'green'}).addTo(mapa);
            }
              else{
        
                  polygon = L.polygon(a, {color: 'blue'}).addTo(mapa);
            }


          // zoom the map to the polygon
               mapa.fitBounds(polygon.getBounds());
               a=[];
            }
          }
    
  }
  }

  mapa.on('click', obKlikuNaMapo);


  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
 
 
  xobj.onload = function () {
    // rezultat ob uspešno prebrani datoteki
    var json = JSON.parse(this.response);
    if (xobj.readyState == 4 && xobj.status >= "200" && xobj.status<"400") {
        
        znacilnosti = json.features;

        for (var i = 0; i < znacilnosti.length; i++) {

          var jeObmocje = 
             typeof(znacilnosti[i].geometry.coordinates[0]) == "object";
              // pridobimo koordinate
          
          var lng = jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][0] : 
              znacilnosti[i].geometry.coordinates[0];
          var lat= jeObmocje ? znacilnosti[i].geometry.coordinates[0][0][1] : 
              znacilnosti[i].geometry.coordinates[1];
              
      
          var naziv = znacilnosti[i].properties.name;
          if(naziv==undefined)
            naziv="";
          var ulica = znacilnosti[i].properties["addr:street"];
          if(ulica==undefined)
            ulica="";
          var hisna_st = znacilnosti[i].properties["addr:housenumber"];
          if(hisna_st==undefined)
            hisna_st="";
          var mesto = znacilnosti[i].properties["addr:city"];
          if(mesto==undefined)
            var naslov = ulica + " " + hisna_st;
          else
            var naslov = ulica + " " + hisna_st +", " + mesto;
          
          for(var j=0;j<znacilnosti[i].geometry.coordinates.length;j++){
            for(var k=0;k<znacilnosti[i].geometry.coordinates[j].length;k++){
              if(!a[k])
                a[k]=[];
              a[k][0] =znacilnosti[i].geometry.coordinates[j][k][1];
              a[k][1] =znacilnosti[i].geometry.coordinates[j][k][0];
            }

            if(a.length>0){
                var polygon = L.polygon(a, {color: 'green'}).addTo(mapa);

          // zoom the map to the polygon
               mapa.fitBounds(polygon.getBounds());
               a=[];
            }
          }
          
      
      dodajMarker(lat,lng, naziv, naslov);
        }
    
}
    else
    console.log('error');
  }
xobj.send();
});

/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku, oris in barvo
 * glede na tip oznake 
 * 
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param naziv naziv, ki se prikaže v oblačku
 * @param naslov naslov, ki se prikaže v oblačku
 * @param tip "blizu","dalec"
 */
function dodajMarker(lat, lng, naziv,naslov) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-icon-2x-red.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  // Ustvarimo marker z vhodnima podatkoma koordinat 
  // in barvo ikone, glede na tip
  var marker = L.marker([lat, lng], {icon: ikona});

  // Izpišemo želeno sporočilo v oblaček
  marker.bindPopup("<div>"+ naziv + "</div></br><div>" + naslov + "</div>").openPopup();

  // Dodamo točko na mapo in v seznam
  marker.addTo(mapa);
  markerji.push(marker);
}